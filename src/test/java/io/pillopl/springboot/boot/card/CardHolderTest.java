package io.pillopl.springboot.boot.card;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class CardHolderTest {

    @Test
    void canWithdraw() {
        //given
        CardHolder cardHolder = aCardHolderWithLimit(BigDecimal.TEN);

        //when
        cardHolder.withdraw(BigDecimal.ONE);

        //then
        assertEquals(new BigDecimal(9), cardHolder.availableLimit());
    }

    @Test
    void cannotWithdrawWhenNotEnoughMoney() {
        //given
        CardHolder cardHolder = aCardHolderWithLimit(BigDecimal.TEN);

        //expect
        assertThrows(CannotWithdrawException.class, () -> cardHolder.withdraw(new BigDecimal(100)));
    }

    @Test
    void cannotWithdrawWhenTooManyWithdrawals() {
        //given
        CardHolder cardHolder = aCardHolderWithLimit(new BigDecimal(100));

        //and
        IntStream.range(1, 46).forEach(i -> cardHolder.withdraw(BigDecimal.ONE));

        //expect
        assertEquals(new BigDecimal(55), cardHolder.availableLimit());
        assertThrows(CannotWithdrawException.class, () -> cardHolder.withdraw(BigDecimal.ONE));
    }

    private CardHolder aCardHolderWithLimit(BigDecimal limit) {
        return new CardHolder(10, limit);
    }

}