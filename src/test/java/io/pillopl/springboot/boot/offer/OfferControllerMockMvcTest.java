package io.pillopl.springboot.boot.offer;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class OfferControllerMockMvcTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    OfferRepository offerRepository;

    @Test
    void shouldReturnAllOffers() throws Exception {
        //given
        thereAreOffers(new Offer(1, 5), new Offer(2, 10));


        //expect
        mockMvc.perform(get("/offers"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().json("[{\"id\":1,\"fee\":5},{\"id\":2,\"fee\":10}]"));
    }

    @Test
    void shouldReturn204WhenNoOffers() throws Exception {
        //given
        thereAreNoOffers();

        //expect
        mockMvc.perform(get("/offers"))
                .andExpect(status().is(204));

    }

    void thereAreNoOffers() {

    }

    void thereAreOffers(Offer... offers) {
        Arrays.asList(offers)
                .forEach(o -> offerRepository.save(o));
    }

}