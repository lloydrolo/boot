package io.pillopl.springboot.boot.offer;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(OfferController.class)
class OfferControllerMockMvcUnitTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    OfferRepository offerRepository;

    @Test
    void shouldReturnAllOffers() throws Exception {
        //given
        thereAreOffers(new Offer(1, 5), new Offer(2, 10));


        //expect
        mockMvc.perform(get("/offers"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().json("[{\"id\":1,\"fee\":5},{\"id\":2,\"fee\":10}]"));
    }

    @Test
    void shouldReturn204WhenNoOffers() throws Exception {
        //given
        thereAreNoOffers();

        //expect
        mockMvc.perform(get("/offers"))
                .andExpect(status().is(204));

    }

    void thereAreNoOffers() {

    }

    void thereAreOffers(Offer... offers) {
        Mockito.when(offerRepository.all()).thenReturn(Arrays.asList(offers));
    }

}