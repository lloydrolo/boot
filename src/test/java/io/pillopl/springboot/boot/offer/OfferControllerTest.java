package io.pillopl.springboot.boot.offer;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class OfferControllerTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @LocalServerPort
    int port;

    @Autowired
    OfferRepository offerRepository;

    @Test
    void shouldReturnAllOffers() {
        //given
        thereAreOffers(new Offer(1, 5), new Offer(2, 10));

        //when
        ResponseEntity<List> response = testRestTemplate.getForEntity("http://localhost:" + port + "/offers", List.class);

        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertEquals(response.getBody().size(), 2);
        assertTrue(response.getHeaders().getValuesAsList("Content-Type").contains("application/json;charset=UTF-8"));

    }

    @Test
    void shouldReturn204WhenNoOffers() {
        //given
        thereAreNoOffers();

        //when
        ResponseEntity<List> response = testRestTemplate.getForEntity("http://localhost:" + port + "/offers", List.class);

        assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);
        assertTrue(!response.getHeaders().getValuesAsList("Content-Type").contains("application/json;charset=UTF-8"));
    }

    void thereAreNoOffers() {

    }

    void thereAreOffers(Offer... offers) {
        Arrays.asList(offers)
                .forEach(o -> offerRepository.save(o));
    }

}