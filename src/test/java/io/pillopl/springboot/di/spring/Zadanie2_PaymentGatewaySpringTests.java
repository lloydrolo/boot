package io.pillopl.springboot.di.spring;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 *  Spraw, aby test przechodził, wykonując kroki:
 *  1. Zastanów się jak najlepiej dodać PaymentGateway do kontekstu Springa
 *      a) podpowiedz: istnieje klasa konfiguracyjna PaymentConfiguration - tam możesz zdefiniować beany
 *      b) adnotacja @ContextConfiguration dla testu mówi z jakiego kontekstu ma korzystać dana klasa testowa
 *  2. Jak zmusić klasę AuthenticationManager, aby zwracała OK/NOT_OK?
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestPaymentConfiguration.class})
public class Zadanie2_PaymentGatewaySpringTests {

    @Autowired
    PaymentGateway paymentGateway;

    @Autowired
    ApplicationContext applicationContext;


    @Test
    public void bramka_platnosci_zwraca_FALSE_bo_uwietrzytelnienie_niepoprawne() {
        //given
        String user = "FRAUD";
        CanAuthenticate bean = applicationContext.getBean("authenticationManager2", CanAuthenticate.class);

        //expect
        assertEquals("FALSE", paymentGateway.pay(user));
    }



}
