package io.pillopl.springboot.di.spring;

public class PaymentDetailsProvider {

    private final PaymentTitleProvider titleProvider;
    private final ClientDetailsProvider clientDetailsProvider;

    public PaymentDetailsProvider(PaymentTitleProvider titleProvider, ClientDetailsProvider clientDetailsProvider) {
        this.titleProvider = titleProvider;
        this.clientDetailsProvider = clientDetailsProvider;
    }

    String getDetailsFor(String userName) {
        return titleProvider.loadTitle(userName) + " " + clientDetailsProvider.loadClientDetails(userName);
    }
}
