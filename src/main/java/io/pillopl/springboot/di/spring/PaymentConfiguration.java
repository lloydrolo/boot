package io.pillopl.springboot.di.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

//@Configuration
public class PaymentConfiguration {

    @Bean
    PaymentGateway paymentGateway(CanAuthenticate authenticationManager) {
        return new PaymentGateway(authenticationManager, new BlueCashService(), new PaymentDetailsProvider(new PaymentTitleProvider(), new ClientDetailsProvider()));
    }

    @Bean
    CanAuthenticate authenticationManager() {
        return new AuthenticationManager();
    }

}
