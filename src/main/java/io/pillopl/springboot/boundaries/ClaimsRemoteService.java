package io.pillopl.springboot.boundaries;

public interface ClaimsRemoteService {

    boolean clientHasNoClaims(BorrowerId borrowerId);
}
