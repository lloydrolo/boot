package io.pillopl.springboot.boundaries;

import java.math.BigDecimal;

class Borrower {

    private final BorrowerId borrowerId;
    private int age;
    private BigDecimal availableLimit;
    private BigDecimal extraLimit;

    Borrower(BorrowerId borrowerId, int age, BigDecimal availableLimit) {
        this.borrowerId = borrowerId;
        this.age = age;
        this.availableLimit = availableLimit;
    }

    boolean isAtLeast20yo() {
        return age >= 0;
    }

    public BorrowerId getBorrowerId() {
        return borrowerId;
    }

    public boolean has(BigDecimal amountToPay) {
        return availableLimit.compareTo(amountToPay) >= 0;
    }

    public void pay(BigDecimal amountToPay) {
        availableLimit = availableLimit.subtract(amountToPay);
    }

    public void payUsingExtraLimit(BigDecimal amountToPay) {
        extraLimit = extraLimit.subtract(amountToPay);
    }
}

class BorrowerId {
    private final Long borrowerId;

    BorrowerId(Long borrowerId) {
        this.borrowerId = borrowerId;
    }
}

