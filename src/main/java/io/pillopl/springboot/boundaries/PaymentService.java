package io.pillopl.springboot.boundaries;


import java.math.BigDecimal;

import static java.util.stream.Collectors.toList;

public class PaymentService {

    private static BigDecimal MIN_AMOUNT_OF_ONE_WITHDRAWAL_TO_BE_VIP = new BigDecimal(100);
    private static int MIN_AMOUNT_OF_WITHDRAWALS_TO_BE_VIP = 10;

    private final BorrowerRepository borrowerRepository;
    private final ClientAddressRemoteService borrowerAddressRemoteService;
    private final BorrowerWithdrawalsRemoteService borrowerWithdrawalsRemoteService;
    private final ClaimsRemoteService claimsRemoteService;


    PaymentService(BorrowerRepository borrowerRepository, ClientAddressRemoteService borrowerAddressRemoteService, BorrowerWithdrawalsRemoteService borrowerWithdrawalsRemoteService, ClaimsRemoteService claimsRemoteService) {
        this.borrowerRepository = borrowerRepository;
        this.borrowerAddressRemoteService = borrowerAddressRemoteService;
        this.borrowerWithdrawalsRemoteService = borrowerWithdrawalsRemoteService;
        this.claimsRemoteService = claimsRemoteService;
    }

    boolean pay(BorrowerId borrowerId, BigDecimal amountToPay) {
        Borrower borrower = borrowerRepository.findById(borrowerId);
        if (canAfford(amountToPay, borrower)) {
            pay(amountToPay, borrower);
            return true;
        } else if (borrowerIsVipClient(borrower)) {
            payUsingExtraLimit(amountToPay, borrower);
            return true;
        } else {
            return false;
        }


    }

    private void pay(BigDecimal amountToPay, Borrower borrower) {
        borrower.pay(amountToPay);
        borrowerWithdrawalsRemoteService.informAboutNewPayment(amountToPay);
    }

    private void payUsingExtraLimit(BigDecimal amountToPay, Borrower borrower) {
        borrower.payUsingExtraLimit(amountToPay);
        borrowerWithdrawalsRemoteService.informAboutNewPayment(amountToPay);
    }

    private boolean canAfford(BigDecimal amountToPay, Borrower borrower) {
        return borrower.has(amountToPay);
    }

    private boolean borrowerIsVipClient(Borrower borrower) {
        return hasEnoughWithdrawals(borrower) && addressIsInEurope(borrower) && isOldEnough(borrower) && noClaimsBy(borrower);
    }

    private boolean noClaimsBy(Borrower borrower) {
        return claimsRemoteService.clientHasNoClaims(borrower.getBorrowerId());
    }

    private boolean isOldEnough(Borrower borrower) {
        return borrowerRepository.findById(borrower.getBorrowerId()).isAtLeast20yo();
    }

    private boolean addressIsInEurope(Borrower borrower) {
        return borrowerAddressRemoteService.getByBorrowerId(borrower.getBorrowerId()).isWithinEurope();
    }

    private boolean hasEnoughWithdrawals(Borrower borrower) {
        return borrowerWithdrawalsRemoteService.getByBorrowerId(borrower.getBorrowerId())
                .stream()
                .filter(w -> w.isMoreThan(MIN_AMOUNT_OF_ONE_WITHDRAWAL_TO_BE_VIP))
                .collect(toList())
                .size() > MIN_AMOUNT_OF_WITHDRAWALS_TO_BE_VIP;
    }

}


