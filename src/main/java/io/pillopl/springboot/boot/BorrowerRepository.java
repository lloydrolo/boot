package io.pillopl.springboot.boot;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
interface BorrowerRepository extends CrudRepository<Borrower, UUID> {

    List<Borrower> findByName(String name);

}
