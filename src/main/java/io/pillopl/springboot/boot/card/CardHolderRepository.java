package io.pillopl.springboot.boot.card;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
interface CardHolderRepository extends JpaRepository<CardHolder, Long> {

    @Query("SELECT wc.comment FROM WithdrawalComment wc")
    List<String> comments();

    Optional<CardHolder> findById(Long borrowerId);
}
