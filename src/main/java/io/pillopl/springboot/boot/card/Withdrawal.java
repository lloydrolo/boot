package io.pillopl.springboot.boot.card;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
class Withdrawal {

    public Withdrawal() {

    }

    @Id
    @GeneratedValue
    private Long id;
    private BigDecimal amount;
    private Instant timestamp;

    @OneToMany(cascade = CascadeType.ALL)
    private List<WithdrawalComment> comments = new ArrayList<>();

    Withdrawal(BigDecimal amount, Instant timestamp) {
        this.amount = amount;
        this.timestamp = timestamp;
    }

    boolean isMoreThan(BigDecimal amount) {
        return this.amount.compareTo(amount) > 0;
    }

    List<WithdrawalComment> getComments() {
        return comments;
    }

    void addComment(String comment) {
        this.comments.add(new WithdrawalComment(comment));
    }
}

