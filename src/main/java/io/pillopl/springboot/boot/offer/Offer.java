package io.pillopl.springboot.boot.offer;

class Offer {

    public Offer() {
    }

    private int id;
    private int fee;

    public Offer(int id, int fee) {

        this.id = id;
        this.fee = fee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }
}
